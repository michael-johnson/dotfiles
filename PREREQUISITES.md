# List of packages used

* i3wm: tiling window manager
* polybar: desktop status bar
* [asus-wmi-sensors](https://github.com/electrified/asus-wmi-sensors): kernel extension to load temp/fan sensors for Asus Prime x470 motherboard
* feh: background settings
* [rofi](https://github.com/davatorium/rofi): app/window launcher/switcher
* lxsession-gtk3: provides /usr/bin/lxpolkit [authentication agent](https://wiki.archlinux.org/title/Polkit#Authentication_agents)
* Flameshot: screen shot utility
