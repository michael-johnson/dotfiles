# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME=""

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment one of the following lines to change the auto-update behavior
# zstyle ':omz:update' mode disabled  # disable automatic updates
# zstyle ':omz:update' mode auto      # update automatically without asking
# zstyle ':omz:update' mode reminder  # just remind me to update when it's time

# Uncomment the following line to change how often to auto-update (in days).
# zstyle ':omz:update' frequency 13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# You can also set it to another string to have that shown instead of the default red dots.
# e.g. COMPLETION_WAITING_DOTS="%F{yellow}waiting...%f"
# Caution: this setting can cause issues with multiline prompts in zsh < 5.7.1 (see #5765)
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
    git
)

# Oh-My-Zsh
source $ZSH/oh-my-zsh.sh

# Headline theme
source $HOME/zsh-themes/headline.zsh-theme

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"
export EDITOR="nvim"
export VISUAL="nvim"
alias vim='nvim'

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
alias ls='ls -l -h --color=auto'
alias df='df -h'
alias lpassc='lpass show -c --password'
alias d='docker'
alias dc='docker-compose'
alias dn='dotnet'
alias g='git'
alias lg='lazygit'
alias zconf="vim $HOME/.zshrc"
alias vconf="vim $HOME/.config/nvim/ --cmd 'cd $HOME/.config/nvim/'"
alias vim='nvim'
alias v='nvim'
alias src="source $HOME/.zshrc"

# see https://stedolan.github.io/jq/manual/#Colors
export JQ_COLORS='0;37:0;37:0;37:0;37:0;36:1;37:1;37'
alias fmt-json-log="$HOME/bin/fmt-json-log.sh"
alias fmt-json-log-dotnet="$HOME/bin/fmt-json-log-dotnet.sh"

# Setup alias git alias for dot file repository
# Per: https://developer.atlassian.com/blog/2016/02/best-way-to-store-dotfiles-git-bare-repo/
alias cnf='git --git-dir=$HOME/.cfg --work-tree=$HOME'

alias lg='lazygit'
alias cnflg='lazygit --git-dir=$HOME/.cfg --work-tree=$HOME'

# Fix gpg signing entry
export GPG_TTY=$(tty)

# Python
alias python='python3'

# Go
export PATH=$PATH:/usr/local/go/bin
export PATH=$PATH:$HOME/go/bin

# NVM
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# Dotnet
export DOTNET_ROOT=/usr/local/share/dotnet
export PATH=$PATH:/usr/local/share/dotnet
export PATH=$PATH:$HOME/.dotnet/tools
export PATH=$PATH:/usr/bin/protoc/bin

# Enable Esc-v to edit command line
autoload -U edit-command-line
# Vi style:
zle -N edit-command-line
bindkey -M vicmd v edit-command-line

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh


# zsh parameter completion for the dotnet CLI
_dotnet_zsh_complete()
{
  local completions=("$(dotnet complete "$words")")

  # If the completion list is empty, just continue with filename selection
  if [ -z "$completions" ]
  then
    _arguments '*::arguments: _normal'
    return
  fi

  # This is not a variable assignment, don't remove spaces!
  _values = "${(ps:\n:)completions}"
}

#compdef _dotnet_zsh_complete dotnet

#export CURL_CA_BUNDLE=/usr/share/ca-certificates/cacert.pem

# add keybindings and completion for fzf (fedora)
# source /etc/bash_completion.d/fzf
#source /usr/share/fzf/shell/key-bindings.zsh

# homebrew
eval "$(/opt/homebrew/bin/brew shellenv)"
export PATH=$PATH:/opt/homebrew/bin

# Rust
[ -f ~/.cargo/env ] && source ~/.cargo/env



## Source confidential changes if present
[ -f ~/.zshrc-confidential ] && source ~/.zshrc-confidential

