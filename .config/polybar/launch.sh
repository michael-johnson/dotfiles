#!/usr/bin/env bash

if ! command -v polybar &> /dev/null
then
    echo "polybar could not be found"
    exit
fi

# Terminate already running bar instances
killall -q polybar
# If all your bars have ipc enabled, you can also use 
# polybar-msg cmd quit

# Launch bar1 and bar2
echo "---" | tee -a /tmp/polybar-default.log
polybar default 2>&1 | tee -a /tmp/polybar-default.log & disown

echo "Bars launched..."
