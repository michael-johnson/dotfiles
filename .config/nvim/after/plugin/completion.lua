local cmp = require'cmp'

-- utility function to deep copy objects
local function copy(obj, seen)
  if type(obj) ~= 'table' then return obj end
  if seen and seen[obj] then return seen[obj] end
  local s = seen or {}
  local res = setmetatable({}, getmetatable(obj))
  s[obj] = res
  for k, v in pairs(obj) do res[copy(k, s)] = copy(v, s) end
  return res
end

local baseKeyMappings = {
  ['<C-k>'] = {
    i = cmp.mapping.select_prev_item(),
    c = cmp.mapping.select_prev_item(),
  },
  ['<C-j>'] = {
    i = cmp.mapping.select_next_item(),
    c = cmp.mapping.select_next_item(),
  },
  ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),
  ['<CR>'] = cmp.mapping.confirm( { select = true} ),
}

local insertKeyMappings = copy(baseKeyMappings)
insertKeyMappings['<Tab>'] = cmp.mapping(function(fallback)
  -- This little snippet will confirm with tab, and if no entry is selected, will confirm the first item
  if cmp.visible() then
    local entry = cmp.get_selected_entry()
    if not entry then
      cmp.select_next_item({ behavior = cmp.SelectBehavior.Select })
    end
    cmp.confirm()
  else
    fallback()
  end
end, {'i','s'})

-- configure default/insert completion
cmp.setup({
  snippet = {
    expand = function(args)
      require("luasnip").lsp_expand(args.body)
    end,
  },
  mapping = insertKeyMappings,
  view = {
    entries = 'custom',
  },
  sources = cmp.config.sources({
    { name = 'nvim_lsp' },
    { name = 'luasnip' },
    { name = 'path' },
    { name = 'nvim_lsp_signature_help' },
    { name = 'omni' },
  }),
})


-- configure command mode completion
local commandKeyMappings = copy(baseKeyMappings)
commandKeyMappings['<Tab>'] = {
  c = function()
    if cmp.visible() then
      local entry = cmp.get_selected_entry()
      if not entry then
        cmp.select_next_item({ behavior = cmp.SelectBehavior.Select })
      end
      cmp.confirm()
    else
      cmp.complete()
    end
  end,
}
cmp.setup.cmdline(':', {
  mapping = commandKeyMappings,
  sources = cmp.config.sources({
    { name = 'cmdline' },
    { name = 'omni' },
  }),
})
