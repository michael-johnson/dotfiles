local nvim_lsp = require('lspconfig')

local on_attach = function(signature_setup, bufnr)
    local bufopts = { noremap=true, silent=true, buffer=bufnr }

    -- show diagnostics as floating window
    vim.api.nvim_create_autocmd("CursorHold", {
      buffer = bufnr,
      callback = function()
        local opts = {
          focusable = false,
          close_events = { "BufLeave", "CursorMoved", "InsertEnter", "FocusLost" },
          border = 'rounded',
          source = 'always',
          prefix = ' ',
          scope = 'cursor',
        }
        vim.diagnostic.open_float(nil, opts)
      end
    })

    vim.keymap.set('n', 'gd', '<cmd>Telescope lsp_definitions<CR>', bufopts)
    vim.keymap.set('n', 'gr', '<cmd>Telescope lsp_references<CR>', bufopts)
    vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
    vim.keymap.set('n', '<leader>d', vim.lsp.buf.hover, bufopts)
    vim.keymap.set('n', '<leader>rn', vim.lsp.buf.rename)
    vim.keymap.set('n', '<leader>ca', vim.lsp.buf.code_action, bufopts)
    vim.keymap.set('n', '<leader>f', function() vim.lsp.buf.format { async = true } end, bufopts)

    vim.keymap.set('n', '<leader>j', function() vim.diagnostic.goto_next { async = true} end, bufopts)
    vim.keymap.set('n', '<leader>k', function() vim.diagnostic.goto_prev { async = true} end, bufopts)
end

local capabilities = require('cmp_nvim_lsp').default_capabilities(vim.lsp.protocol.make_client_capabilities())

local add_lsp = function(name, settings)
    nvim_lsp[name].setup {
        capabilities = capabilities,
        on_attach = on_attach,
        settings = settings,
    }
end

-- Registor language servers
-- ref: https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md
add_lsp("gopls")
add_lsp("yamlls")
add_lsp("csharp_ls")
add_lsp("eslint")

-- Typescript
nvim_lsp.tsserver.setup {
  name = "tsserver",
  cmd = { "typescript-language-server", "--stdio" },
  filetypes = { "javascript", "javascriptreact", "javascript.jsx", "typescript", "typescriptreact", "typescript.tsx" },
  init_options = { hostInfo = "neovim" },
  -- root_dir = root_pattern("tsconfig.json", "package.json", "jsconfig.json", ".git"),
  single_file_support = true,
}

-- PHP
nvim_lsp.intelephense.setup {
  name = "intelephense",
  cmd = { "intelephense", "--stdio" },
  cmd_env = {
    NODE_OPTIONS = "--max_old_space_size=4096",
  },
  capabilities = capabilities,
  on_attach = on_attach,
  filetypes = { "php" },
  init_options = {
    storagePath = "./.vscode/intelephense-cache"
  },
  settings = {
    intelephense = {
      files = {
        exclude = {
          "**/lib/app/config/config.di.compilation.memoizedAnnotationArgs.php",
          "**/lib/include/mpdf/mpdf.php",
          "**/.git/**",
          "**/.svn/**",
          "**/.hg/**",
          "**/CVS/**",
          "**/.DS_Store/**",
          "**/node_modules/**",
          "**/bower_components/**",
          "**/dbserver/**",
          "**/taskserver/ONE-TIME-FIXES/**",
          "**/thirdparty/**",
          "**/docs/**",
          "**/manualcom/**",
          "**/online/assets/generated/**",
          "**/toplevel/**",
        },
        maxSize = 15000000
      },
      stubs = {
        "amqp",
        "apache",
        "apcu",
        "bcmath",
        "blackfire",
        "bz2",
        "calendar",
        "cassandra",
        "com_dotnet",
        "Core",
        "couchbase",
        "crypto",
        "ctype",
        "cubrid",
        "curl",
        "date",
        "dba",
        "decimal",
        "dom",
        "ds",
        "enchant",
        "Ev",
        "event",
        "exif",
        "fann",
        "FFI",
        "ffmpeg",
        "fileinfo",
        "filter",
        "fpm",
        "ftp",
        "gd",
        "gearman",
        "geoip",
        "geos",
        "gettext",
        "gmagick",
        "gmp",
        "gnupg",
        "grpc",
        "hash",
        "http",
        "ibm_db2",
        "iconv",
        "igbinary",
        "imagick",
        "imap",
        "inotify",
        "interbase",
        "intl",
        "json",
        "judy",
        "ldap",
        "leveldb",
        "libevent",
        "libsodium",
        "libxml",
        "lua",
        "lzf",
        "mailparse",
        "mapscript",
        "mbstring",
        "mcrypt",
        "memcache",
        "memcached",
        "meminfo",
        "meta",
        "ming",
        "mongo",
        "mongodb",
        "mosquitto-php",
        "mqseries",
        "msgpack",
        "mssql",
        "mysql",
        "mysql_xdevapi",
        "mysqli",
        "ncurses",
        "newrelic",
        "oauth",
        "oci8",
        "odbc",
        "openssl",
        "parallel",
        "Parle",
        "pcntl",
        "pcov",
        "pcre",
        "pdflib",
        "PDO",
        "pdo_ibm",
        "pdo_mysql",
        "pdo_pgsql",
        "pdo_sqlite",
        "pgsql",
        "Phar",
        "phpdbg",
        "posix",
        "pspell",
        "pthreads",
        "radius",
        "rar",
        "rdkafka",
        "readline",
        "recode",
        "redis",
        "Reflection",
        "regex",
        "rpminfo",
        "rrd",
        "SaxonC",
        "session",
        "shmop",
        "SimpleXML",
        "snmp",
        "soap",
        "sockets",
        "sodium",
        "solr",
        "SPL",
        "SplType",
        "SQLite",
        "sqlite3",
        "sqlsrv",
        "ssh2",
        "standard",
        "stats",
        "stomp",
        "suhosin",
        "superglobals",
        "svn",
        "sybase",
        "sync",
        "sysvmsg",
        "sysvsem",
        "sysvshm",
        "tidy",
        "tokenizer",
        "uopz",
        "uv",
        "v8js",
        "wddx",
        "win32service",
        "winbinder",
        "wincache",
        "wordpress",
        "xcache",
        "xdebug",
        "xhprof",
        "xml",
        "xmlreader",
        "xmlrpc",
        "xmlwriter",
        "xsl",
        "xxtea",
        "yaf",
        "yaml",
        "yar",
        "zend",
        "Zend OPcache",
        "ZendCache",
        "ZendDebugger",
        "ZendUtils",
        "zip",
        "zlib",
        "zmq",
        "zookeeper"
      },
      maxMemory = 10000,
      telemetry = { enabled = false },
      format = { enable = true }
    }
  }
}

-- Lua
nvim_lsp.lua_ls.setup {
  on_init = function(client)
    local path = client.workspace_folders[1].name
    if not vim.loop.fs_stat(path..'/.luarc.json') and not vim.loop.fs_stat(path..'/.luarc.jsonc') then
      client.config.settings = vim.tbl_deep_extend('force', client.config.settings, {
        Lua = {
          runtime = {
            -- Tell the language server which version of Lua you're using
            -- (most likely LuaJIT in the case of Neovim)
            version = 'LuaJIT'
          },
          -- Make the server aware of Neovim runtime files
          workspace = {
            checkThirdParty = false,
            library = {
              vim.env.VIMRUNTIME
              -- "${3rd}/luv/library"
              -- "${3rd}/busted/library",
            }
            -- or pull in all of 'runtimepath'. NOTE: this is a lot slower
            -- library = vim.api.nvim_get_runtime_file("", true)
          }
        }
      })

      client.notify("workspace/didChangeConfiguration", { settings = client.config.settings })
    end
    return true
  end
}
