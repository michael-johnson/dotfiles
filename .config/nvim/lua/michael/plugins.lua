local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable', -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

local plugins = {
    -- search
	'nvim-lua/plenary.nvim', -- required for other stuff
	'nvim-telescope/telescope.nvim', -- file and others searcher
    -- file browser
	{
	    'stevearc/oil.nvim',
	    opts = {},
	},
    -- make it purdy
	'vim-airline/vim-airline', -- status bar
	'vim-airline/vim-airline-themes', -- status bar themes
	{
	    "overcache/NeoSolarized",
	    lazy = false, -- make sure we load this during startup if it is your main colorscheme
	    priority = 1000, -- make sure to load this before all the other start plugins
	    config = function()
	      vim.cmd [[ set background=dark ]]
	      vim.cmd [[ set termguicolors ]]
	      vim.cmd [[ colorscheme NeoSolarized ]]
	    end
	},
    -- git
	'airblade/vim-gitgutter', -- git integration
	'tpope/vim-fugitive', -- more git integration
    -- debug
	'mfussenegger/nvim-dap', -- Debug Adapter Protocol, a framework for using debuggers
	'rcarriga/nvim-dap-ui', -- a UI for nvim-dap
    -- formatters
	{'rhysd/vim-fixjson', lazy = true, ft = 'json' }, -- json formatter
    -- language features
	'neovim/nvim-lspconfig', -- configs for Nvim LSP
	{'nvim-treesitter/nvim-treesitter', build=':TSUpdate'}, -- configs for treesitter
	'nvim-treesitter/nvim-treesitter-context', -- context top bar using treesitter
	'towolf/vim-helm', -- Helm chart syntax
    -- completion
	{
	    'hrsh7th/nvim-cmp', -- completion handler
	    event = { "InsertEnter", "CmdlineEnter" },
	},
	'hrsh7th/cmp-nvim-lsp', -- LSP completion
	'hrsh7th/cmp-buffer', -- buffer completion
	'hrsh7th/cmp-path', -- path completion
	'hrsh7th/cmp-nvim-lsp-signature-help', -- function signatures from LSP
	'hrsh7th/cmp-cmdline', -- command mode completion
	'hrsh7th/cmp-omni', -- omnifunc completion
    -- incremental rename
	'smjonas/inc-rename.nvim',
    -- snippets
	'L3MON4D3/LuaSnip', -- snippets handler
	'rafamadriz/friendly-snippets', -- collection of snippets
    -- databases
	{
	    'kristijanhusak/vim-dadbod-ui',
	    dependencies = {
	      { 'tpope/vim-dadbod', lazy = true },
	      { 'kristijanhusak/vim-dadbod-completion', ft = { 'sql', 'mysql', 'plsql' }, lazy = true },
	    },
	    cmd = {
	      'DBUI',
	      'DBUIToggle',
	      'DBUIAddConnection',
	      'DBUIFindBuffer',
	    },
	    init = function()
	      -- Your DBUI configuration
	      vim.g.db_ui_use_nerd_fonts = 1
	    end,
	},
    -- misc.
	'tpope/vim-commentary', -- quick comment/un-comment toggles
	'tpope/vim-sleuth', -- auto set indent width, etc based on context
	{
	    "folke/todo-comments.nvim", -- highlight todo comments
	    dependencies = { "nvim-lua/plenary.nvim" },
	    opts = {
		gui_style = {
		    fg = "NONE", -- The gui style to use for the fg highlight group.
		    bg = "BOLD", -- The gui style to use for the bg highlight group.
		},
		colors = {
		    error = { "DiagnosticError", "ErrorMsg", "#DC2626" },
		    warning = { "DiagnosticWarn", "WarningMsg", "#FBBF24" },
		    info = { "DiagnosticInfo", "#2563EB" },
		    hint = { "DiagnosticHint", "#10B981" },
		    default = { "Identifier", "#7C3AED" },
		    test = { "Identifier", "#FF00FF" }
		},
		highlight = {
		    multiline = true, -- enable multine todo comments
		    multiline_pattern = "^.", -- lua pattern to match the next multiline from the start of the matched keyword
		    multiline_context = 10, -- extra lines that will be re-evaluated when changing a line
		    before = "bg", -- "fg" or "bg" or empty
		    keyword = "wide_bg", -- "fg", "bg", "wide", "wide_bg", "wide_fg" or empty. (wide and wide_bg is the same as bg, but will also highlight surrounding characters, wide_fg acts accordingly but with fg)
		    after = "bg", -- "fg" or "bg" or empty
		    pattern = [[.*<(KEYWORDS)\s*:]], -- pattern or table of patterns, used for highlighting (vim regex)
		    comments_only = true, -- uses treesitter to match keywords in comments only
		    max_line_len = 400, -- ignore lines longer than this
		    exclude = {}, -- list of file types to exclude highlighting
		},
		keywords = {
		    FIX = {
		    icon = "T ", -- icon used for the sign, and in search results
		    color = "error", -- can be a hex color, or a named color (see below)
		    alt = { "FIXME", "BUG", "FIXIT", "ISSUE" }, -- a set of other keywords that all map to this FIX keywords
		    -- signs = false, -- configure signs for some keywords individually
		    },
		    TODO = { icon = "T ", color = "info" },
		    HACK = { icon = "T ", color = "warning" },
		    WARN = { icon = "T ", color = "warning", alt = { "WARNING", "XXX" } },
		    PERF = { icon = "T ", alt = { "OPTIM", "PERFORMANCE", "OPTIMIZE" } },
		    NOTE = { icon = "T ", color = "hint", alt = { "INFO" } },
		    TEST = { icon = "T ", color = "test", alt = { "TESTING", "PASSED", "FAILED" } },
		},
	    }
	},
}

local opts = {};
require('lazy').setup(plugins, opts)

require('dapui').setup()
