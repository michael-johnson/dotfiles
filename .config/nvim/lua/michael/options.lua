local v = vim
local opt = v.opt

opt.updatetime=100

-- Colors
v.cmd('AirlineTheme solarized')

-- Disable mouse. What am I? A heathen?
opt.mouse=''

-- Line numbers
opt.relativenumber = true
opt.number = true
opt.cursorline = true

-- Indentation
opt.autoindent = true
opt.smartindent = true
opt.expandtab = true
opt.tabstop = 4
opt.shiftwidth = 4
opt.softtabstop = 4

-- Formatting
vim.g.fixjson_fix_on_save = 0 -- don't auto format json on save

-- Keep ten line buffer from cursor to edge of screen
opt.scrolloff = 10
opt.scrolloff = 10

-- Don't wrap text
opt.wrap = false

-- Swapfile/Undo History
opt.swapfile = false
opt.backup = false
opt.undodir = os.getenv("HOME") .. "/.config/nvim_undodir"
opt.undofile = true
opt.undolevels = 9999

-- Show/highlight trailing whitespace
opt.list = true
opt.listchars = "tab:  ,trail:·"

-- Enable incremental search/replace
opt.hlsearch = false
opt.incsearch = true

-- Ignore case when searching, unless upper-case is used
opt.ignorecase = true
opt.smartcase = true

-- Set language (makes `sort l` collate properly)
vim.api.nvim_exec('set spell spelllang=en_us', true)

--v.cmd('syntax enable')
opt.visualbell = true

-- Diagnostics
vim.diagnostic.config({
  virtual_text = false,
  signs = true,
  underline = true,
  update_in_insert = true,
  severity_sort = true,
})
