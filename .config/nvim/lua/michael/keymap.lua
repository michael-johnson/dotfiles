local k = vim.keymap

-- hj or jh to exit insert mode
k.set('i', 'hj', '<Esc>');
k.set('i', 'jh', '<Esc>');

-- Telescope Searching
local builtin = require('telescope.builtin')
k.set('n', '<leader>ff', builtin.find_files)
k.set('n', '<leader>fg', builtin.live_grep)
k.set('n', '<leader>fb', builtin.buffers)
k.set('n', '<leader>fh', builtin.help_tags)
k.set('n', '<leader>gs', builtin.git_status)
k.set('n', '<leader>fe', builtin.diagnostics)

-- Oil File Browser
local oil = require('oil')
k.set('n', '<leader>e', oil.open)

-- Git gutter
k.set('n', '<C-j>', '<Plug>(GitGutterNextHunk)')
k.set('n', '<C-k>', '<Plug>(GitGutterPrevHunk)')
k.set('n', '<leader>ga', '<Plug>(GitGutterStageHunk)')
k.set('n', '<leader>u', '<Plug>(GitGutterUndoHunk)')

-- Debugging
local dap = require('dap')
k.set('n', '<leader>b', dap.toggle_breakpoint, {})
k.set('n', '<F9>', dap.continue, {})
k.set('n', '<F8>', dap.step_over, {})
k.set('n', '<F7>', dap.step_into, {})
local dapui = require('dapui')
k.set('n', '<F12>', dapui.toggle)

-- Commentary
k.set('n', '<C-_>', '<Plug>Commentary', { noremap = true }) -- Control+/
k.set('v', '<C-_>', '<Plug>Commentary', { noremap = true }) -- Control+/

-- Rename
k.set('n', '<leader>r', function()
  return ":IncRename " .. vim.fn.expand("<cword>") -- fills current word
end, { expr = true })

-- Diagnostics
k.set('n', ']d', function()
    vim.diagnostic.goto_next()
    vim.cmd('norm zz')
end)
k.set('n', '[d', function()
    vim.diagnostic.goto_prev()
    vim.cmd('norm zz')
end)

-- Signature Display
k.set('n', '<leader>d', vim.lsp.buf.signature_help, opts)
-- k.set({'n'}, '', function()
--     require('lsp_signature').toggle_float_win()
-- end, { silent = true, noremap = true, desc = 'toggle signature' })

